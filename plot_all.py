import numpy
import sys
import matplotlib.pyplot as plt
import pandas as pd
import glob
from io import StringIO


filename = sys.argv[1]


if sys.argv[1] == 'spectra':

    filenames = sorted(glob.glob(filename + '/*'))
    writer = pd.ExcelWriter(filename + '_all_data.xlsx', engine='xlsxwriter')

    for i,f in enumerate(filenames):
        
        #creating pdfs

        data = numpy.genfromtxt(fname=f, delimiter='\t')
        fig = plt.figure()
        plt.plot(data[:,0],data[:,1])
        plt.ylabel('Intensity(AU)')
        plt.xlabel('Wavelength (nm)')
        plt.savefig('pictures/spectra_plot{}.pdf'.format(i+1))



        #plotting in excel

        df = pd.read_csv(f, delimiter='\t', header=None, error_bad_lines=False)

        col_names = ['Wavelength (nm)', 'Intensity (a.u.)']
        df.rename(columns={0: col_names[0], 1: col_names[1]}, inplace=True)
    
    
        df.to_excel(writer, sheet_name='Sheet{}'.format(i+1))
	

    writer.save()

elif sys.argv[1] == 'bending':

    writer = pd.ExcelWriter(filename + '_all_data.xlsx', engine='xlsxwriter')
    filenames = sorted(glob.glob(filename + '/*'))

for i,f in enumerate(filenames):
        data = open(f) 
        lines = data.read().split('"Specimen"')

        
        for n in range(1, len(lines)):
            df = pd.read_csv(StringIO(lines[n]), sep=',', skiprows=18)
            
            x = df['Strain [Exten.] %']
            y = df['Stress MPa']

            fig = plt.figure()

            plt.plot(x,y)
            plt.ylabel('Stress Mpa')
            plt.xlabel('Strain')

            #renaming files
            file_renamed = [i for i in f.split('_')]

            file_renamed = file_renamed[-1].split('.')[0]

            print(file_renamed)

            plt.savefig('pictures/bending_plot{}{}.pdf'.format(file_renamed,n))

            #plotting excel
            x = numpy.array(x)
            y = numpy.array(y)
            exceldata = pd.DataFrame({'Strain':x, 'Stress MPa':y}, columns=['Strain', 'Stress MPa'])

            exceldata.to_excel(writer, sheet_name='{}{}'.format(file_renamed,n))
	
            
writer.save()

