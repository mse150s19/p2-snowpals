##Code Overview

The code is made to create excel plots and pictures (as pdfs) for data in spectra/ and bending/


##Running The Code

To run the code for spectra:

    $ python plot_all.py spectra

To run the code for bending:

    $ python plot_all.py bending


##Understanding The Code

when sysargv[1] is spectra:

The pdfs are first created and saved in the pictures directory. These files are named spectra_plot_(number_that_corresponds_with_the_data_from_spectra) 

The excel spreadsheet is created with the name spectra_all_plots.xlsx


when sysargv[1] is bending:

The pdfs are first created and saves in pictures directory. The naming shows bending, material type, and the data number (number of data groups in the .raw file)

The excel spreadsheet is created with the name bending_all_plots.xlsx


##Opening Files

If libreoffice is downloaded, the excel file can be opened with the command:

    $ open example.xlsx



## Things To Note
plot_all.py assumes that you have a pictures directory.

Make sure there are no .pdf files in the spectra folder
